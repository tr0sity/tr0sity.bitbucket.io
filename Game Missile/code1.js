gdjs.MainCode = {};
gdjs.MainCode.stopDoWhile2 = false;

gdjs.MainCode.GDVilleObjects1= [];
gdjs.MainCode.GDVilleObjects2= [];
gdjs.MainCode.GDmeteoreObjects1= [];
gdjs.MainCode.GDmeteoreObjects2= [];
gdjs.MainCode.GDbgObjects1= [];
gdjs.MainCode.GDbgObjects2= [];
gdjs.MainCode.GDmissileObjects1= [];
gdjs.MainCode.GDmissileObjects2= [];
gdjs.MainCode.GDscoreGameObjects1= [];
gdjs.MainCode.GDscoreGameObjects2= [];
gdjs.MainCode.GDboomObjects1= [];
gdjs.MainCode.GDboomObjects2= [];
gdjs.MainCode.GDheartObjects1= [];
gdjs.MainCode.GDheartObjects2= [];
gdjs.MainCode.GDstarting_95textObjects1= [];
gdjs.MainCode.GDstarting_95textObjects2= [];
gdjs.MainCode.GDrubbleObjects1= [];
gdjs.MainCode.GDrubbleObjects2= [];

gdjs.MainCode.conditionTrue_0 = {val:false};
gdjs.MainCode.condition0IsTrue_0 = {val:false};
gdjs.MainCode.condition1IsTrue_0 = {val:false};


gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects1Objects = Hashtable.newFrom({"meteore": gdjs.MainCode.GDmeteoreObjects1});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects2Objects = Hashtable.newFrom({"meteore": gdjs.MainCode.GDmeteoreObjects2});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDVilleObjects2Objects = Hashtable.newFrom({"Ville": gdjs.MainCode.GDVilleObjects2});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects2Objects = Hashtable.newFrom({"meteore": gdjs.MainCode.GDmeteoreObjects2});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDrubbleObjects2Objects = Hashtable.newFrom({"rubble": gdjs.MainCode.GDrubbleObjects2});gdjs.MainCode.eventsList0x71a44c = function(runtimeScene) {

}; //End of gdjs.MainCode.eventsList0x71a44c
gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDboomObjects1Objects = Hashtable.newFrom({"boom": gdjs.MainCode.GDboomObjects1});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDboomObjects1Objects = Hashtable.newFrom({"boom": gdjs.MainCode.GDboomObjects1});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects1Objects = Hashtable.newFrom({"meteore": gdjs.MainCode.GDmeteoreObjects1});gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDVilleObjects1Objects = Hashtable.newFrom({"Ville": gdjs.MainCode.GDVilleObjects1});gdjs.MainCode.eventsList0xb4be0 = function(runtimeScene) {

{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDheartObjects1.createFrom(runtimeScene.getObjects("heart"));
gdjs.MainCode.GDrubbleObjects1.createFrom(runtimeScene.getObjects("rubble"));
{for(var i = 0, len = gdjs.MainCode.GDheartObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDheartObjects1[i].pauseAnimation();
}
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "meteoreTimer");
}{for(var i = 0, len = gdjs.MainCode.GDrubbleObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDrubbleObjects1[i].hide();
}
}}

}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "a");
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDstarting_95textObjects1.createFrom(runtimeScene.getObjects("starting_text"));
{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "meteoreTimer");
}{for(var i = 0, len = gdjs.MainCode.GDstarting_95textObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDstarting_95textObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "meteoreTimer");
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDmeteoreObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects1Objects, gdjs.random(1920), 0, "");
}{for(var i = 0, len = gdjs.MainCode.GDmeteoreObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDmeteoreObjects1[i].addForceTowardPosition(gdjs.random(1920), 1200, 120, 1);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "meteoreTimer");
}{for(var i = 0, len = gdjs.MainCode.GDmeteoreObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDmeteoreObjects1[i].setAngle(gdjs.randomWithStep(90, 360, 15));
}
}}

}


{



}


{


gdjs.MainCode.stopDoWhile2 = false;
do {gdjs.MainCode.GDVilleObjects2.createFrom(runtimeScene.getObjects("Ville"));
gdjs.MainCode.GDheartObjects2.createFrom(runtimeScene.getObjects("heart"));
gdjs.MainCode.GDmeteoreObjects2.createFrom(runtimeScene.getObjects("meteore"));
gdjs.MainCode.GDrubbleObjects2.createFrom(runtimeScene.getObjects("rubble"));
gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects2Objects, gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDVilleObjects2Objects, false, runtimeScene, false);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects2Objects, gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDrubbleObjects2Objects, false, runtimeScene, false);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.MainCode.GDrubbleObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDrubbleObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.MainCode.GDheartObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDheartObjects2[i].setAnimationFrame(gdjs.MainCode.GDheartObjects2[i].getAnimationFrame() + (1));
}
}{for(var i = 0, len = gdjs.MainCode.GDVilleObjects2.length ;i < len;++i) {
    gdjs.MainCode.GDVilleObjects2[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents: 
gdjs.MainCode.eventsList0x71a44c(runtimeScene);} //Subevents end.
}
} else gdjs.MainCode.stopDoWhile2 = true; 
} while ( !gdjs.MainCode.stopDoWhile2 );

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MainCode.condition0IsTrue_0.val) {
gdjs.MainCode.GDboomObjects1.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDboomObjects1Objects, gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), "");
}}

}


{

gdjs.MainCode.GDboomObjects1.createFrom(runtimeScene.getObjects("boom"));
gdjs.MainCode.GDmeteoreObjects1.createFrom(runtimeScene.getObjects("meteore"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDboomObjects1Objects, gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDmeteoreObjects1Objects, false, runtimeScene, false);
}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDmeteoreObjects1 */
gdjs.MainCode.GDscoreGameObjects1.createFrom(runtimeScene.getObjects("scoreGame"));
{for(var i = 0, len = gdjs.MainCode.GDmeteoreObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDmeteoreObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}{for(var i = 0, len = gdjs.MainCode.GDscoreGameObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDscoreGameObjects1[i].setString("Score:" + " " + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}}

}


{

gdjs.MainCode.GDboomObjects1.createFrom(runtimeScene.getObjects("boom"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MainCode.GDboomObjects1.length;i<l;++i) {
    if ( gdjs.MainCode.GDboomObjects1[i].hasAnimationEnded() ) {
        gdjs.MainCode.condition0IsTrue_0.val = true;
        gdjs.MainCode.GDboomObjects1[k] = gdjs.MainCode.GDboomObjects1[i];
        ++k;
    }
}
gdjs.MainCode.GDboomObjects1.length = k;}if (gdjs.MainCode.condition0IsTrue_0.val) {
/* Reuse gdjs.MainCode.GDboomObjects1 */
{for(var i = 0, len = gdjs.MainCode.GDboomObjects1.length ;i < len;++i) {
    gdjs.MainCode.GDboomObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.MainCode.GDVilleObjects1.createFrom(runtimeScene.getObjects("Ville"));

gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.MainCode.mapOfGDgdjs_46MainCode_46GDVilleObjects1Objects) == 0;
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "");
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "gameOver", true);
}}

}


{



}


{


gdjs.MainCode.condition0IsTrue_0.val = false;
{
gdjs.MainCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "x");
}if (gdjs.MainCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}}

}


}; //End of gdjs.MainCode.eventsList0xb4be0


gdjs.MainCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MainCode.GDVilleObjects1.length = 0;
gdjs.MainCode.GDVilleObjects2.length = 0;
gdjs.MainCode.GDmeteoreObjects1.length = 0;
gdjs.MainCode.GDmeteoreObjects2.length = 0;
gdjs.MainCode.GDbgObjects1.length = 0;
gdjs.MainCode.GDbgObjects2.length = 0;
gdjs.MainCode.GDmissileObjects1.length = 0;
gdjs.MainCode.GDmissileObjects2.length = 0;
gdjs.MainCode.GDscoreGameObjects1.length = 0;
gdjs.MainCode.GDscoreGameObjects2.length = 0;
gdjs.MainCode.GDboomObjects1.length = 0;
gdjs.MainCode.GDboomObjects2.length = 0;
gdjs.MainCode.GDheartObjects1.length = 0;
gdjs.MainCode.GDheartObjects2.length = 0;
gdjs.MainCode.GDstarting_95textObjects1.length = 0;
gdjs.MainCode.GDstarting_95textObjects2.length = 0;
gdjs.MainCode.GDrubbleObjects1.length = 0;
gdjs.MainCode.GDrubbleObjects2.length = 0;

gdjs.MainCode.eventsList0xb4be0(runtimeScene);
return;

}
gdjs['MainCode'] = gdjs.MainCode;
