gdjs.gameOverCode = {};
gdjs.gameOverCode.GDGameoverObjects1= [];
gdjs.gameOverCode.GDGameoverObjects2= [];
gdjs.gameOverCode.GDScoreObjects1= [];
gdjs.gameOverCode.GDScoreObjects2= [];
gdjs.gameOverCode.GDrestartObjects1= [];
gdjs.gameOverCode.GDrestartObjects2= [];
gdjs.gameOverCode.GDNewObjectObjects1= [];
gdjs.gameOverCode.GDNewObjectObjects2= [];

gdjs.gameOverCode.conditionTrue_0 = {val:false};
gdjs.gameOverCode.condition0IsTrue_0 = {val:false};
gdjs.gameOverCode.condition1IsTrue_0 = {val:false};


gdjs.gameOverCode.eventsList0xb4be0 = function(runtimeScene) {

{


gdjs.gameOverCode.condition0IsTrue_0.val = false;
{
gdjs.gameOverCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.gameOverCode.condition0IsTrue_0.val) {
gdjs.gameOverCode.GDScoreObjects1.createFrom(runtimeScene.getObjects("Score"));
{for(var i = 0, len = gdjs.gameOverCode.GDScoreObjects1.length ;i < len;++i) {
    gdjs.gameOverCode.GDScoreObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}
}}

}


{


gdjs.gameOverCode.condition0IsTrue_0.val = false;
{
gdjs.gameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "a");
}if (gdjs.gameOverCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", true);
}}

}


{


gdjs.gameOverCode.condition0IsTrue_0.val = false;
{
gdjs.gameOverCode.condition0IsTrue_0.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "x");
}if (gdjs.gameOverCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.stopGame(runtimeScene);
}}

}


}; //End of gdjs.gameOverCode.eventsList0xb4be0


gdjs.gameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.gameOverCode.GDGameoverObjects1.length = 0;
gdjs.gameOverCode.GDGameoverObjects2.length = 0;
gdjs.gameOverCode.GDScoreObjects1.length = 0;
gdjs.gameOverCode.GDScoreObjects2.length = 0;
gdjs.gameOverCode.GDrestartObjects1.length = 0;
gdjs.gameOverCode.GDrestartObjects2.length = 0;
gdjs.gameOverCode.GDNewObjectObjects1.length = 0;
gdjs.gameOverCode.GDNewObjectObjects2.length = 0;

gdjs.gameOverCode.eventsList0xb4be0(runtimeScene);
return;

}
gdjs['gameOverCode'] = gdjs.gameOverCode;
