gdjs.splashCode = {};
gdjs.splashCode.GDsplashObjects1= [];
gdjs.splashCode.GDsplashObjects2= [];

gdjs.splashCode.conditionTrue_0 = {val:false};
gdjs.splashCode.condition0IsTrue_0 = {val:false};
gdjs.splashCode.condition1IsTrue_0 = {val:false};


gdjs.splashCode.eventsList0xb4be0 = function(runtimeScene) {

{


gdjs.splashCode.condition0IsTrue_0.val = false;
{
gdjs.splashCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 4, "splash");
}if (gdjs.splashCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "");
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Main", true);
}}

}


{


gdjs.splashCode.condition0IsTrue_0.val = false;
{
gdjs.splashCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.splashCode.condition0IsTrue_0.val) {
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}}

}


}; //End of gdjs.splashCode.eventsList0xb4be0


gdjs.splashCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.splashCode.GDsplashObjects1.length = 0;
gdjs.splashCode.GDsplashObjects2.length = 0;

gdjs.splashCode.eventsList0xb4be0(runtimeScene);
return;

}
gdjs['splashCode'] = gdjs.splashCode;
